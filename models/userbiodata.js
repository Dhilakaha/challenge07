'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserBiodata.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'userId'
      });
    }
  };
  UserBiodata.init({
    user_id: DataTypes.INTEGER,
    firstName: DataTypes.STRING,
    birthdate: DataTypes.DATE,
    rank: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'UserBiodata',
  });
  return UserBiodata;
};