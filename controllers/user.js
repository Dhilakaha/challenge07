const User = require('../models').User;
const UserBiodata = require('../models').UserBiodata;
const UserHistory = require('../models').UserHistory;

module.exports = {
  list(req, res) {
    return User
      .findAll({
        include: [{
          model: UserHistory,
          as: 'userhistory'
        }],
        order: [
          ['createdAt', 'DESC'],
          [{ model: UserHistory, as: 'userhistory' }, 'createdAt', 'DESC'],
        ],
      })
      .then((user) => res.status(200).send(user))
      .catch((error) => { res.status(400).send(error); });
  },

  getById(req, res) {
    return User
      .findByPk(req.params.id, {
        include: [{
          model: UserHistory,
          as: 'userhistory'
        }],
      })
      .then((user) => {
        if (!user) {
          return res.status(404).send({
            message: 'user Not Found',
          });
        }
        return res.status(200).send(user);
      })
      .catch((error) => {
        console.log(error);
        res.status(400).send(error);
      });
  },
};