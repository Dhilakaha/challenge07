var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let data = require('./src/data.json');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/api/v1/images', (req, res) => {
  res.status(200).json(data);
});

app.get('/home', (req, res) => {
  res.sendFile('./src/index.html', { root: __dirname });
})

app.get('/login', (req, res) => {
  res.sendFile('./src/login.html', { root: __dirname });
})

app.get('/register', (req, res) => {
  res.sendFile('./src/register.html', { root: __dirname });
})

app.get('/game', (req, res) => {
  res.sendFile('./src/game.html', { root: __dirname });
})

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
